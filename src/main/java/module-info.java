module com.example.TODOLIST {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires jasypt;
    requires mysql.connector.j;


    opens com.example.TODOLIST to javafx.fxml;
    exports com.example.TODOLIST;
    exports com.example.TODOLIST.view;
    opens com.example.TODOLIST.view to javafx.fxml;
}