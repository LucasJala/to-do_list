package com.example.TODOLIST;
import org.jasypt.util.password.rfc2307.RFC2307MD5PasswordEncryptor;

public class PasswordEncryptor {
    private static final RFC2307MD5PasswordEncryptor passwordEncryptor = new RFC2307MD5PasswordEncryptor();

    /**
     * Encapsula o forma de criptografia utilizada do Jasypt
     */
    private static String encryptPassword(String password) {
        return passwordEncryptor.encryptPassword(password);
    }
    public static String encrypt(String password) {
        return encryptPassword(password);
    }
}
