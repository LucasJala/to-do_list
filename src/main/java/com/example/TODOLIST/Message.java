package com.example.TODOLIST;
/**
 * Enumeração para mensagens do sistema.
 * @author Lucas Sampaio
 */
public enum Message {
    SUCCESS("Success Operation"),
    SUCCESS_CREATE_USER("User created successfully!"),
    SUCCESS_EDIT_USER("User edited successfully!"),
    SUCCESS_DELETE_USER("User deleted successfully!"),
    ERROR_PASSWORD_NOT_SAME("Passwords don't match!"),
    SUCCESS_LOGIN("Login successfully!"),
    SUCCESS_EDIT_TASK("Task edited successfully!"),
    SUCCESS_CREATE_TASK("Task created successfully!"),
    ERROR("Error"),
    ERROR_LOGIN("Invalid email or password!"),
    ERROR_EMPTY_FIELD("Fill in all fields!"),
    ERROR_EMAIL("Invalid Email!"),
    ERROR_USER_EXIST("User already exists!"),
    ERROR_CREATE_USER("User not created, please try again!"),
    ERROR_DELETE_USER("User not deleted, please try again!"),
    ERROR_USER_NOT_EXIST("User not found, please try again!"),
    ERROR_PASSWORD_LENGTH("Password must have\n" +
            "-11 characters\n" +
            "-At least one digit\n" +
            "-At least one lowercase letter\n" +
            "-At least one uppercase letter\n" +
            "-At least one special character\n" ),
    ASK_DELETE_USER("Are you sure you want to delete this user?"),
    CANCEL("Operation Cancelled"),
    ERROR_UPDATE_USER("User not updated, please try again!");
    private final String message;
    Message(String s) {
        message = s;
    }
    public String getMessage() {
        return message;
    }
}
