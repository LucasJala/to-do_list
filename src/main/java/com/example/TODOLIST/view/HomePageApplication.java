package com.example.TODOLIST.view;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;

public class HomePageApplication extends Application {
    private String email;
    public HomePageApplication(String email){
        this.email = email;
    }
    @Override
    public void start(Stage stage) throws IOException, SQLException {
        FXMLLoader fxmlLoader = new FXMLLoader(RegisterApplication.class.getResource("/com/example/TODOLIST/HomePage-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        //Recebe o controller do fxml para enviar o Email do usuário
        HomePageController homePageController = fxmlLoader.getController();
        homePageController.receiveEmail(email);
        stage.setTitle("HomePage");
        stage.setScene(scene);
        stage.show();
    }
}
