package com.example.TODOLIST.view;
import com.example.TODOLIST.HelloApplication;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.controller.AccountController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterController {
    @FXML
    private TextField txtname;
    @FXML
    private TextField txtlogin;
    @FXML
    private PasswordField txtpassword;
    @FXML
    private PasswordField txtconfirmpassword;
    @FXML
    private AnchorPane ap;
    private AccountController accountController = new AccountController();
    private Message result;

    @FXML
    protected void register() throws IOException, SQLException {
        String name = txtname.getText().toString();
        String login = txtlogin.getText().toString();
        String password = txtpassword.getText().toString();
        String confirmPassword = txtconfirmpassword.getText().toString();
        Alert alert;

        result = accountController.createUser(name, login, password, confirmPassword);
        if (result == Message.SUCCESS_CREATE_USER){
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText(result.getMessage());
            alert.show();
            Stage stage = (Stage) ap.getScene().getWindow();
            new HelloApplication().start(stage);
        }else{
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(result.getMessage());
            alert.show();
        }
        result = null;
    }
    public void returnToLogin(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ap.getScene().getWindow();
        new HelloApplication().start(stage);
    }
}
