package com.example.TODOLIST.view;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.controller.TaskController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AddTaskController {
    private String email;
    @FXML
    private TextField txttarefa;
    @FXML
    private TextField txtdescricao;
    @FXML
    private DatePicker date;
    private String txtdata;

    public void receiveEmail(String email){this.email = email; }
    public void AddTask() throws SQLException {
        String task = txttarefa.getText().toString();
        String description   = txtdescricao.getText().toString();
        Alert alerta;

        if(task.isEmpty() || description.isEmpty() || txtdata == null){
            alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle(Message.ERROR.getMessage());
            alerta.setContentText(Message.ERROR_EMPTY_FIELD.getMessage());
        }else{
            TaskController taskController = new TaskController();
            taskController.createTask(email, task, description, txtdata);
            alerta = new Alert(Alert.AlertType.INFORMATION);
            alerta.setTitle(Message.SUCCESS.getMessage());
            alerta.setContentText(Message.SUCCESS_CREATE_TASK.getMessage());
        }
        alerta.show();
    }
    public void date(ActionEvent actionEvent) {
        LocalDate dataSelecionada = date.getValue();
        txtdata = dataSelecionada.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
}
