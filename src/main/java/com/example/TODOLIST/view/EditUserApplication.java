package com.example.TODOLIST.view;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class EditUserApplication extends Application {
    private final String email;
    public EditUserApplication(String email) {
        this.email = email;
    }
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(EditUserApplication.class.getResource("/com/example/TODOLIST/editUser-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        EditUserController editUserController = fxmlLoader.getController();
        editUserController.receiveEmail(email);
        stage.setTitle("Edit User");
        stage.setScene(scene);
        stage.show();
    }
}
