package com.example.TODOLIST.view;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.controller.TaskController;
import com.example.TODOLIST.entities.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class EditTaskController {
    private Task task;
    @FXML
    private TextField txttask;
    @FXML
    private TextField txtdescription;
    @FXML
    private DatePicker date;
    private TaskController taskController;
    @FXML
    private String formattedDate;

    public EditTaskController(){
        this.taskController  = new TaskController();
    }

    public void setTask(Task task){
        this.task = task;
        txttask.setText(task.getNameTask());
        txtdescription.setText(task.getDescription());
        formattedDate = task.getDate();
        date.setValue(LocalDate.parse(formattedDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
    public void editTask() throws SQLException {
        String taskName = txttask.getText().toString();
        String description = txtdescription.getText().toString();
        Alert alert;

        if(taskName.isEmpty() || description.isEmpty() || formattedDate.isEmpty()){
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(Message.ERROR_EMPTY_FIELD.getMessage());
        }else{
            taskController.editTask(task.getId(), taskName, description, formattedDate);
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText(Message.SUCCESS_EDIT_TASK.getMessage());
        }
        alert.show();
    }
    public void date(ActionEvent actionEvent) {
        LocalDate dateChosen = date.getValue();
        formattedDate = dateChosen.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
    }
}
