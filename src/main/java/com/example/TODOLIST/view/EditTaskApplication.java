package com.example.TODOLIST.view;
import com.example.TODOLIST.HelloApplication;
import com.example.TODOLIST.entities.Task;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class EditTaskApplication extends Application {
    private Task task;
    public EditTaskApplication(Task task){this.task = task;}
    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("/com/example/TODOLIST/editTask-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        EditTaskController editTaskController = fxmlLoader.getController();
        editTaskController.setTask(task);

        stage.setTitle("Edit Task");
        stage.setScene(scene);
        stage.show();
    }
}
