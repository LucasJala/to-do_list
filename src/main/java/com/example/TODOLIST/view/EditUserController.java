package com.example.TODOLIST.view;
import com.example.TODOLIST.HelloApplication;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.controller.AccountController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class EditUserController {
    private String email;
    private AccountController accountController = new AccountController();
    @FXML
    private TextField txtname;
    @FXML
    private TextField txtemail;
    @FXML
    private PasswordField txtpassword;
    @FXML
    private PasswordField txtconfirmpassword;
    @FXML
    private AnchorPane ap;
    private Alert alert;
    private Message messageResult;
    public void receiveEmail(String email) {
        this.email = email;
    }
    public void deleteuser(ActionEvent actionEvent) throws SQLException, IOException {
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete Confirmation");
        alert.setContentText(Message.ASK_DELETE_USER.getMessage());

        ButtonType botaoSim = new ButtonType("yes");
        ButtonType botaoNao = new ButtonType("no");
        alert.getButtonTypes().setAll(botaoSim, botaoNao);

        // Exibe o diálogo de confirmação e aguarda a resposta do usuário
        Optional<ButtonType> resultado = alert.showAndWait();
        alert = new Alert(Alert.AlertType.INFORMATION);

        // Verifica se o usuário confirmou a exclusão
        if (resultado.get() == botaoSim) {
            messageResult = accountController.deleteUser(email);
            if (messageResult == Message.SUCCESS_DELETE_USER) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(messageResult.getMessage());
                alert.show();
                email = null;
                Stage stage = (Stage) ap.getScene().getWindow();
                new HelloApplication().start(stage);
            }else{
                alert.setContentText(messageResult.getMessage());
                alert.show();
            }
        }else{
            alert.setContentText(Message.CANCEL.getMessage());
            alert.show();
        }
    }
    public void editconfirm(ActionEvent actionEvent) throws IOException, SQLException {
        String name = txtname.getText().toString();
        String newEmail = txtemail.getText().toString();
        String password = txtpassword.getText().toString();
        String confirmPassword = txtconfirmpassword.getText().toString();
        messageResult = accountController.updateUser(name, email, password, newEmail, confirmPassword);
        alert = new Alert(Alert.AlertType.INFORMATION);

        if (messageResult == Message.SUCCESS_EDIT_USER){
            email = newEmail;
        }
        alert.setContentText(messageResult.getMessage());
        alert.show();
    }
    public void backHomepage (ActionEvent actionEvent) throws IOException, SQLException {
        Stage stage = (Stage) ap.getScene().getWindow();
        new HomePageApplication(email).start(stage);
    }
}
