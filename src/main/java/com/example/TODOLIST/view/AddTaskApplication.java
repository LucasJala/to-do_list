package com.example.TODOLIST.view;
import com.example.TODOLIST.HelloApplication;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class AddTaskApplication  extends Application {
    private String email;
    public AddTaskApplication(String email){this.email = email;}
    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("/com/example/TODOLIST/addTask-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        AddTaskController addTaskController = fxmlLoader.getController();
        addTaskController.receiveEmail(email);

        stage.setTitle("Add Task");
        stage.setScene(scene);
        stage.show();
    }
}
