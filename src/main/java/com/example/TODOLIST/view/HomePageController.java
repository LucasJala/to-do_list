package com.example.TODOLIST.view;
import com.example.TODOLIST.HelloApplication;
import com.example.TODOLIST.controller.AccountController;
import com.example.TODOLIST.controller.TaskController;
import com.example.TODOLIST.entities.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
public class HomePageController {

    private String email;
    private String userName;
    private ContextMenu contextMenu;
    private AccountController accountController = new AccountController();
    private ArrayList<Task> tasks;
    private Task selectedTask;
    @FXML
    private AnchorPane ap;
    @FXML
    private Label labeluser;
    private TaskController taskController;
    @FXML
    private TextArea descriptionTextArea;
    @FXML
    private ListView<Task> eventList;
    private ObservableList<Task> list = FXCollections.observableArrayList();

    public HomePageController() throws SQLException {
       this.taskController = new TaskController();
    }

    public void initialize() {
        eventList.setItems(list);
    }
    public void receiveEmail(String email) throws SQLException {
        this.email = email;
        RefreshHomepage();
    }
    /**
     * Atualiza a página inicial carregando as tarefas do usuário e atualizando a lista de tarefas e dados do
     * usuario na interface.
     */
    private void RefreshHomepage() throws SQLException {
        tasks = taskController.carregarTasks(email);
        list.clear();
        list.addAll(tasks);
        list.sort(Comparator.comparing(Task::getDate));
        eventList.getItems().removeAll();
        eventList.setItems(list);
        eventList.refresh();
        userName = accountController.getNameUser(email);
        labeluser.setText(userName);
    }
    /**
     * Abre uma nova interface para adicionar tarefas e atualiza a lista de tarefas na página inicial após a adição.
     */
    @FXML
    private void addTask() throws IOException, SQLException {
        Stage stage = new Stage();
        new AddTaskApplication(email).start(stage);
        // Atualiza a lista quando a nova tarefa for adicionada
        stage.setOnCloseRequest(event -> {
            try { RefreshHomepage();} catch (SQLException e) {throw new RuntimeException(e);}
        });
    }
    /**
     * Recupera a descrição da tarefa selecionada na lista de eventos com o mouse e exibe-a na JTextArea
     */
    @FXML
    public void showDescriptionLeftClick(javafx.scene.input.MouseEvent mouseEvent) {
        // Fecha o menu de contexto, se estiver aberto
        if (contextMenu != null && contextMenu.isShowing()) {
            contextMenu.hide();
        }
        // Obtém a tarefa selecionada na lista de eventos
        Task selectedTask = eventList.getSelectionModel().getSelectedItem();
        // Verifica se uma tarefa foi selecionada
        if (selectedTask != null) {
            String description = selectedTask.getDescription();
            descriptionTextArea.setText(description);
        }
    }
    /**
     * Exibe o menu de contexto na posição do mouse quando o usuário clica com o botão direito
     */
    @FXML
    public void showContextMenu(javafx.scene.input.ContextMenuEvent contextMenuEvent)  {
        MenuItem editItem;
        MenuItem deleteItem;
        MenuItem doneItem;
        selectedTask = eventList.getSelectionModel().getSelectedItem();

        if (contextMenu == null) {
            contextMenu = new ContextMenu();
            //Opção de editar
            editItem = new MenuItem("Edit Task");
            editItem.setOnAction(event -> {
                try {handleEditTask();
                } catch (IOException e) { throw new RuntimeException(e);}
            });
            //Opção de deletar
            deleteItem = new MenuItem("Delete Task");
            deleteItem.setOnAction(event -> { try { handleDeleteTask(selectedTask);
            } catch (SQLException e) {throw new RuntimeException(e);}});
            // Opção mudar status
            doneItem = new MenuItem("Change Status");
            doneItem.setOnAction(event -> {
                try { changeTaskStatus(selectedTask);
                } catch (SQLException e) {throw new RuntimeException(e);}
            });
            // Adiciona os itens ao menu de contexto
            contextMenu.getItems().addAll(editItem, deleteItem, doneItem);
        }
        // Mostra o menu de contexto na posição do mouse
        contextMenu.show(eventList, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
    }
    private void handleEditTask() throws IOException {
        Stage stage = new Stage();
        new EditTaskApplication(selectedTask).start(stage);
        // Define um evento para atualizar a página inicial quando a janela de edição for fechada
        stage.setOnCloseRequest(event -> {
            try { RefreshHomepage();} catch (SQLException e) {throw new RuntimeException(e);}
        });
    }
    private void handleDeleteTask(Task selectedTask) throws SQLException {
        // Verifica se uma tarefa foi selecionada
        if (selectedTask != null) {
            // Deleta a tarefa utilizando o taskController
            taskController.deletarTask(selectedTask.getId());
            // Atualiza a página inicial após a exclusão da tarefa
            RefreshHomepage();
        }
    }
    public void changeTaskStatus(Task selectedTask) throws SQLException {
        // Altera o status da tarefa chamando o método changeStatus do taskController
        try{ taskController.changeStatus(selectedTask.getId(), !selectedTask.isDone());
        } catch (SQLException e) {throw new RuntimeException(e);
        }
        // Atualiza a página inicial após a alteração do status da tarefa
        RefreshHomepage();
    }
    public void showUserSettings(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage) ap.getScene().getWindow();
        new EditUserApplication(email).start(stage);
    }
    public void refreshHomepage(ActionEvent actionEvent) throws Exception { RefreshHomepage();}
    public void logoffUser(ActionEvent actionEvent) throws IOException {
        Stage stage = (Stage) ap.getScene().getWindow();
        new HelloApplication().start(stage);
    }
}
