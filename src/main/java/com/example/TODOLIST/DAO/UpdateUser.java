package com.example.TODOLIST.DAO;

import java.sql.SQLException;

public interface UpdateUser {
    public boolean update(String email, String name, String password, String newEmail) throws SQLException;
    public void close() throws SQLException;
}
