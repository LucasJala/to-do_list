package com.example.TODOLIST.DAO;
import com.example.TODOLIST.entities.User;
import java.sql.ResultSet;
import java.sql.SQLException;
public class UserDAO implements ConsultUser, CreateUser, DeleteUser, UpdateUser {
    private ConectionDB conectionDB;
    private ResultSet result;
    private String sql;
    private int lines;
    public UserDAO() {
        try{
            this.conectionDB = new ConectionDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public boolean create(User user) throws SQLException {
        String email = user.getEmail();
        String name = user.getName();
        String password = user.getPassword();
        try{
            sql = "INSERT INTO Users (email, name, password) VALUES ('"+email+"', '"+name+"', '"+password+"');";;
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return lines > 0;
    }
    public boolean loginUser(String email, String password) throws SQLException {
        sql = "SELECT email FROM Users WHERE email = '"+email+"' AND password = '"+password+"';";
        try{
            result = this.conectionDB.getS().executeQuery(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return result.next();
    }
    public String selectName(String email) throws SQLException {
        sql = "SELECT name FROM Users WHERE email = '"+email+"';";
        try{
            result = this.conectionDB.getS().executeQuery(sql);
            if (result.next()) { // Verifica se existe um registro retornado
                return result.getString("name");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public boolean update(String email, String name, String password, String newEmail) throws SQLException {
        sql = "UPDATE Users SET email = '"+newEmail+"', name = '"+name+"', password = '"+password+"' WHERE email = '"+email+"';";

        try {
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return lines > 0;
    }
    public boolean delete(String email) throws SQLException {
        sql = "DELETE FROM Users WHERE email = '"+email+"';";
        try{
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return lines > 0;
    }
    public void close() throws SQLException {
        this.conectionDB.close();
    }
}
