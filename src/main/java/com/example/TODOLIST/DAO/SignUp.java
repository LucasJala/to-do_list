package com.example.TODOLIST.DAO;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.PasswordEncryptor;
import com.example.TODOLIST.Validation.IValidator;
import com.example.TODOLIST.Validation.UserValidator;
import com.example.TODOLIST.entities.User;
import java.sql.SQLException;
public class SignUp {
    private User user;
    private CreateUser createUser;
    private Message messageResult;
    private IValidator validator;
    public SignUp(){
        this.validator = new UserValidator();
        this.user = new User();
    }
    /**
     * Cria um usuário na base de dados com o nome, email e senha fornecidos.
     */
    public Message signUpUser(String name, String email, String password, String confirmPassword) throws SQLException {
        this.createUser = new UserDAO();
        boolean userCreated;
        messageResult = validator.signUpUser(name, email, password, confirmPassword);
        if(messageResult != Message.SUCCESS){
            return messageResult;
        }
        String encryptedPassword = PasswordEncryptor.encrypt(password);
        user.setEmail(email);
        user.setName(name);
        user.setPassword(encryptedPassword);
        userCreated = createUser.create(user);
        createUser.close();

        if(userCreated){
            messageResult = Message.SUCCESS_CREATE_USER;
        }else{
            messageResult = Message.ERROR_CREATE_USER;
        }
        return messageResult;
    }
}
