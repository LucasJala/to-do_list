package com.example.TODOLIST.DAO;

import com.example.TODOLIST.entities.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TaskDAO implements ITaskDAO {
    private ConectionDB conectionDB;
    private int lines;
    public TaskDAO() {
        try{
            this.conectionDB = new ConectionDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void close() throws SQLException {
        this.conectionDB.close();
    }
    public void createTask(String email, String task, String descricao, String data) throws SQLException {
        try{
            String sql = "INSERT INTO Tasks (nameTask ,descr, date, email_user) VALUES ('"+task+"','"+descricao+"', '"+data+"','"+email+"');";
            this.conectionDB.getS().execute(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public int updateStatus(int id, boolean status) throws SQLException {
        int statusInt = status ? 1 : 0;  // Convertendo o valor booleano para int
        String sql = "UPDATE Tasks SET done = "+statusInt+" WHERE id = "+id+";";
        int lines = 0;
        try{
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        close();
        return lines;
    }
    public int updateValues(int id, String taskName, String descricao, String date) throws SQLException {
        String sql = "UPDATE Tasks SET nameTask = '"+taskName+"', descr = '"+descricao+"', date = '"+date+"' WHERE id = "+id+";";
        lines = 0;
        try{
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return lines;
    }
    public ArrayList<Task> selectAll(String email) throws SQLException {
        ArrayList<Task> tasks = new ArrayList<>();
        Task task;
        try{
            String sql = "SELECT * FROM Tasks WHERE email_user = '"+email+"';";
            ResultSet rs = this.conectionDB.getS().executeQuery(sql);
            while(rs.next()){
                task = new Task();
                task.setId(rs.getInt("id"));
                task.setDescription(rs.getString("descr"));
                task.setNameTask(rs.getString("nameTask"));
                task.setDate(rs.getString("date"));
                task.setUserEmail(rs.getString("email_user"));
                task.setStatus(rs.getBoolean("done"));
                tasks.add(task);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return tasks;
    }
    public int delete(int id) throws SQLException {
        String sql = "DELETE FROM Tasks WHERE id = "+id+";";
        lines = 0;
        try{
            lines = this.conectionDB.getS().executeUpdate(sql);
        }catch (Exception e){
            e.printStackTrace();
        }
        return lines;
    }
}
