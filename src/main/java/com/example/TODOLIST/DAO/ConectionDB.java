package com.example.TODOLIST.DAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConectionDB {
    /*
     * Classe para estabelecer a conexão com o BD.
     * Connection : Classe para permitir a estabelecimento da conexão
     * Statement : Classe para permitir a execução de comandos SQL
     */
    private Connection Conn; // Referencia a conexão do BD
    private Statement s; // Referencia ao objeto do tipo Statement
    /*
     * Tenta carregar a classe driver JDBC para MySQL,
     * para conseguir criar a conexão
     */
    public ConectionDB(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            //Modifique os valores para o seu BD
            this.Conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/ToDo", "lucas", "85178254");
            // Cria um objeto do tipo Statement para permitir a execução de comandos SQL.
            this.s = Conn.createStatement();
        }catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    public void close() throws SQLException {
        try{
            s.close(); // fechar o objeto Statement
            Conn.close(); // Encerra a conexão com o BD
        }catch(SQLException ex){
        }
    }
    public Statement getS() {
        return s;
    }
}
