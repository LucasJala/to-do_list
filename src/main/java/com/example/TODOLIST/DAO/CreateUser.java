package com.example.TODOLIST.DAO;

import com.example.TODOLIST.entities.User;

import java.sql.SQLException;

public interface CreateUser {
    public boolean create(User user) throws SQLException;
    public void close() throws SQLException;
}
