package com.example.TODOLIST.DAO;

import java.sql.SQLException;

public interface DeleteUser {
    public boolean delete(String email) throws SQLException;
    public void close() throws SQLException;
}
