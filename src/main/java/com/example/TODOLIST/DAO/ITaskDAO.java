package com.example.TODOLIST.DAO;

import com.example.TODOLIST.entities.Task;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ITaskDAO {
    public void createTask(String email, String task, String descricao, String data) throws SQLException;
    public int updateStatus(int id, boolean status) throws SQLException;
    public ArrayList<Task> selectAll(String email) throws SQLException;
    public int updateValues(int id, String taskName, String descricao, String date) throws SQLException;
    public int delete(int id) throws SQLException;

    public void close() throws SQLException;
}
