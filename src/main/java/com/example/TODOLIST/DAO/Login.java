package com.example.TODOLIST.DAO;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.PasswordEncryptor;
import com.example.TODOLIST.Validation.IValidator;
import com.example.TODOLIST.Validation.UserValidator;
import java.sql.SQLException;
public class Login {
    private ConsultUser Account;
    private IValidator validator;
    private Message messageResult;
    public Login(){
        this.validator = new UserValidator();
        this.Account = new UserDAO();
    }
    /**
     * Realiza o login do usuário com o email e senha fornecidos.
     */
    public Message loginUser(String email, String password) throws SQLException {
        boolean loginUser;
        messageResult = validator.loginUser(email, password);

        if(messageResult != Message.SUCCESS){
            return messageResult;
        }
        String encryptedPassword = PasswordEncryptor.encrypt(password);
        loginUser = Account.loginUser(email, encryptedPassword);
        if(loginUser){
            messageResult = Message.SUCCESS_LOGIN;
        }else{
            messageResult = Message.ERROR_LOGIN;
        }
        Account.close();
        return messageResult;
    }
}
