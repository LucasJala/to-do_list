package com.example.TODOLIST.DAO;

import java.sql.SQLException;

public interface ConsultUser {
    public String selectName(String email) throws SQLException;
    public boolean loginUser(String email, String password) throws SQLException;
    public void close() throws SQLException;
}
