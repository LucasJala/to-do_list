package com.example.TODOLIST.entities;

public class Task {
    private int id;
    private String nameTask;
    private String date;
    private String description;
    private boolean status = false; // Por padrão uma tarefa ainda não foi concluída
    private String userEmail;

    public String getNameTask() {
        return nameTask;
    }
    public void setNameTask(String nameTask) {
        this.nameTask = nameTask;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String formartedStatus() {
        if (status)
            return "Done";
        return "In progress";
    }
    public String getUserEmail() {
        return userEmail;
    }
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
    public String toString(){
        return "Task: " + nameTask + "\nDue: " + date + "\nStatus: " + formartedStatus();
    }
}
