package com.example.TODOLIST.controller;
import com.example.TODOLIST.DAO.ITaskDAO;
import com.example.TODOLIST.DAO.TaskDAO;
import com.example.TODOLIST.entities.Task;
import java.sql.SQLException;
import java.util.ArrayList;
public class TaskController {
    private ITaskDAO taskDAO = new TaskDAO();

    public void createTask(String email, String task, String descricao, String data) throws SQLException {
        taskDAO.createTask(email, task, descricao, data);
        taskDAO.close();
    }
    public ArrayList<Task> carregarTasks(String email) throws SQLException {
        taskDAO = new TaskDAO();
        ArrayList<Task> tasks = taskDAO.selectAll(email);
        taskDAO.close();
        return tasks;
    }
    public boolean deletarTask(int id) throws SQLException {
        taskDAO = new TaskDAO();
        int lines = taskDAO.delete(id);
        taskDAO.close();
        return lines > 0;
    }
    public void changeStatus(int id, boolean status) throws SQLException {
        taskDAO = new TaskDAO();
        taskDAO.updateStatus(id, status);
        taskDAO.close();
    }
    public void editTask(int id, String taskName, String descricao, String date) throws SQLException {
        taskDAO = new TaskDAO();
        taskDAO.updateValues(id, taskName, descricao, date);
        taskDAO.close();
    }
}
