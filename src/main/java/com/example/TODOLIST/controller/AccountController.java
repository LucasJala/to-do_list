package com.example.TODOLIST.controller;
import com.example.TODOLIST.DAO.*;
import com.example.TODOLIST.Message;
import com.example.TODOLIST.PasswordEncryptor;
import com.example.TODOLIST.Validation.IValidator;
import com.example.TODOLIST.Validation.UserValidator;
import java.sql.SQLException;

/**
 * Classe responsável por gerenciar as operações envolvendo a conta do usuário.
 *
 * @author Lucas Sampaio
 */
public class AccountController {
    private SignUp signUp;
    private Login login;
    private DeleteUser deleteUser;
    private UpdateUser updateUser;
    private ConsultUser consultUser;
    private Message messageResult;
    public AccountController(){
        signUp = new SignUp();
        login = new Login();
    }
    /**
     * Cria um novo usuário com o nome, email e senha fornecidos.
     *
     * @param name o nome do usuário a ser criado.
     * @param email o email do usuário a ser criado.
     * @param password a senha do usuário a ser criado.
     * @return uma mensagem de resultado da operação.
     */
    public Message createUser(String name, String email, String password, String confirmPassword) throws SQLException {
        return signUp.signUpUser(name, email, password, confirmPassword);
    }
    /**
     * Realiza o login do usuário com o email e senha fornecidos.
     *
     * @param email o email do usuário para login
     * @param password a senha do usuário para login
     * @return uma mensagem de resultado da operação
     */
    public Message loginUser(String email, String password) throws SQLException {
        return login.loginUser(email, password);
    }
    /**
     * Exclui o usuário associado ao email fornecido.
     *
     * @param email o email do usuário a ser excluído
     * @return uma mensagem de resultado da operação
     */
    public Message deleteUser(String email) throws SQLException {
        deleteUser = new UserDAO();
        if(deleteUser.delete(email)){
            messageResult = Message.SUCCESS_DELETE_USER;
        }else{
            messageResult = Message.ERROR_DELETE_USER ;
        }
        deleteUser.close();
        return messageResult;
    }
    /**
     * Atualiza as informações do usuário, incluindo nome, email e senha.
     *
     * @param name o novo nome do usuário
     * @param email o email atual do usuário
     * @param password a senha atual do usuário
     * @param newEmail o novo email para atualização
     */
    public Message updateUser(String name, String email, String password, String newEmail, String confirmPassword) throws SQLException {
        IValidator validator = new UserValidator();
        updateUser = new UserDAO();
        messageResult = validator.editUser(name, newEmail, password, confirmPassword);
        if(messageResult != Message.SUCCESS){
            return messageResult;
        }
        String encryptedPassword = PasswordEncryptor.encrypt(password);
        if(updateUser.update(email, name, encryptedPassword, newEmail)){
            messageResult = Message.SUCCESS_EDIT_USER;
        }else {
            messageResult = Message.ERROR_UPDATE_USER;
        }
        updateUser.close();
        return messageResult;
    }
    public String getNameUser(String email) throws SQLException {
        consultUser = new UserDAO();
        String name = consultUser.selectName(email);
        consultUser.close();
        return name;
    }
}
