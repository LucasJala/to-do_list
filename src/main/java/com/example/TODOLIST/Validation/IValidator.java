package com.example.TODOLIST.Validation;

import com.example.TODOLIST.Message;

import java.sql.SQLException;

public interface IValidator {
    public Message signUpUser(String name, String email, String password, String confirmPassword) throws SQLException;
    public Message loginUser(String email, String password) throws SQLException;
    public Message editUser(String newEmail, String name, String password, String confirmPassword) throws SQLException;
    public Message validateFields(String email, String name, String password);
}
