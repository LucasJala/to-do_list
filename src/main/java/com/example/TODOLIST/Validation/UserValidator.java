package com.example.TODOLIST.Validation;
import com.example.TODOLIST.DAO.ConsultUser;
import com.example.TODOLIST.DAO.UserDAO;
import com.example.TODOLIST.Message;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Classe responsável por validar as informações do usuário.
 */
public class UserValidator implements IValidator {
    private ConsultUser consultUser;
    private Message resultMessage;
    /**
     * Valida os campos de nome, email e senha.
     *
     * @param name o nome do usuário
     * @param email o email do usuário
     * @param password a senha do usuário
     * @return uma mensagem de resultado da validação
     */
    public Message validateFields(String name, String email, String password) {
        if(!notEmpty(name, email, password)){
            return Message.ERROR_EMPTY_FIELD;
        } else if (!strongPassword(password)) {
            return Message.ERROR_PASSWORD_LENGTH;
        } else if (!ValidateEmail(email)) {
            return Message.ERROR_EMAIL;
        }
        return Message.SUCCESS;
    }
    public Message signUpUser(String name, String email, String password, String confirmPassword) throws SQLException {
       if (samePassword(password, confirmPassword)) {
           // Valida os campos de nome, email e senha
           resultMessage = validateFields(name, email, password);
           if (resultMessage != Message.SUCCESS) {
               return resultMessage;
           }
           // Verifica se o usuário já existe no banco de dados
           if (userExists(email)) {
               resultMessage = Message.ERROR_USER_EXIST;
           }
       } else {
           resultMessage = Message.ERROR_PASSWORD_NOT_SAME;
       }
        return resultMessage;
    }
    public Message loginUser(String email, String password) throws SQLException {
        resultMessage = validateFields("", email, password);
        if(resultMessage != Message.SUCCESS){
            return resultMessage;
        }
        if (!userExists(email)) {
            resultMessage = Message.ERROR_USER_NOT_EXIST;
        }
        return resultMessage;
    }
    public Message editUser(String name,String newEmail, String password, String confirmPassword) throws SQLException {
        if(samePassword(password, confirmPassword)) {
            resultMessage = validateFields(name, newEmail, password);
            if (resultMessage != Message.SUCCESS) {
                return resultMessage;
            }
            if (userExists(newEmail)) {
                resultMessage = Message.ERROR_USER_EXIST;
            }
        }else {
            resultMessage = Message.ERROR_PASSWORD_NOT_SAME;
        }
        return resultMessage;
    }
    private boolean notEmpty(String name, String email, String password) {
        return name != null && email != null && password != null;
    }
    private boolean samePassword(String password, String confirmPassword) throws SQLException {
        return password.equals(confirmPassword);
    }
    /**
     * Verifica se a senha atende aos critérios de segurança.
     *
     * @param password a senha a ser verificada
     * @return true se a senha atende aos critérios de segurança, caso contrário false
     */
    private boolean strongPassword(String password) {
        return password.length() >= 11 &&  // Pelo menos 12 caracteres
                password.matches(".*[0-9].*") && // Pelo menos um dígito
                password.matches(".*[a-z].*") && // Pelo menos uma letra minúscula
                password.matches(".*[A-Z].*") && // Pelo menos uma letra maiúscula
                password.matches(".*[!@#$%^&*()_+=].*"); // Pelo menos um caractere especial
    }
    /**
     * Valida se o formato do email fornecido é válido.
     *
     * @param email o email a ser validado
     * @return true se o email estiver em um formato válido, caso contrário false
     */
    private boolean ValidateEmail(String email) {
        // Expressão regular para validar o formato do email
        String regex = "^[\\w\\.-]+@[a-zA-Z\\d\\.-]+\\.[a-zA-Z]{2,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /**
     * Verifica se um usuário com o email fornecido já existe no banco de dados.
     *
     * @param email o email do usuário a ser verificado
     * @return true se o usuário existe, caso contrário false
     * @throws SQLException se ocorrer um erro durante a consulta no banco de dados
     */
    private boolean userExists(String email) throws SQLException {
        consultUser = new UserDAO();
        String result = consultUser.selectName(email);
        consultUser.close();
        return result != null;
    }
}
