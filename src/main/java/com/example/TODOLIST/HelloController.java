package com.example.TODOLIST;
import com.example.TODOLIST.controller.AccountController;
import com.example.TODOLIST.view.HomePageApplication;
import com.example.TODOLIST.view.RegisterApplication;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;
import java.sql.SQLException;

public class HelloController {
    @FXML
    private TextField txtlogin;
    @FXML
    private PasswordField txtsenha;
    @FXML
    private AnchorPane ap;
    private Message message;
    private final AccountController accountController = new AccountController();

    @FXML
    protected void Login() throws IOException, SQLException {
        String login = txtlogin.getText().toString();
        String pwd = txtsenha.getText().toString();
        message = accountController.loginUser(login, pwd);
        // Verifica se o login foi bem-sucedido
        if(message == Message.SUCCESS_LOGIN){
            // Se sim, abre a página inicial do aplicativo
            Stage stage = (Stage) ap.getScene().getWindow();
            new HomePageApplication(login).start(stage);
        }else{
            // Se não, exibe uma mensagem de erro ao usuário
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setContentText(message.getMessage());
            alerta.show();
        }
    }
    @FXML
    protected void register() throws IOException {
        Stage stage = (Stage) ap.getScene().getWindow();
        new RegisterApplication().start(stage);
        }
}