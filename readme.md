# Todo List Application

Este é um aplicativo desenvolvido para a tarefa final da disciplina de Banco de Dados 2. Seu objetivo é permitir aos usuários gerenciar suas tarefas de forma clara e eficiente, mantendo o registro de tarefas e usuários de forma persistente em um banco de dados.

## 🔧 Execução

Para testar a aplicação utilize o usuario de testes abaixo:

**Login:** teste1@gmail.com
**Senha:** P@ssw0rd123

Para executar o programa, siga estas etapas:

### 1. Configuração do Banco de Dados

Seu programa Java está configurado para se conectar a um banco de dados MySQL. Para configurar a conexão com o banco de dados, siga estas etapas:

a. Certifique-se de ter o servidor MySQL instalado e em execução na sua máquina local ou em um servidor acessível.

b. Abra o arquivo de código-fonte que contém a configuração da conexão com o banco de dados DAO -> ConexaoDB.

c. Procure pelo metodo public ConexaoDB() ou clique aqui -> 
[Link para ConectionDB.java](src/main/java/com/example/TODOLIST/DAO/ConectionDB.java)
, onde está presente o DriverManager.getConnection() responsavel por configurar a conexão. 

d. Modifique a URL de conexão (`jdbc:mysql://127.0.0.1:3306/ToDo`), o nome de usuário (`lucas`) e a senha (`123`) de acordo com as configurações do seu banco de dados MySQL.

   Por exemplo:
   ```java
   DriverManager.getConnection("jdbc:mysql://seu_endereco_do_servidor:porta/ToDo", "seu_nome_de_usuario", "sua_senha");
   ```
### 2. Importando o Script do Banco de Dados

Seu aplicativo requer a criação do banco de dados e tabelas:

a. Certifique-se de ter o servidor MySQL instalado e em execução na sua máquina local ou em um servidor acessível.

b. Encontre o arquivo de script do banco de dados presente em TODOLIST/Script SQL/TODOLIST.sql. Este arquivo geralmente tem uma extensão `.sql`.

c. O script SQL contem os comandos necessários para criar o SCHEMA do banco de dados, suas tabelas e registros de testes.

c. Use a interface do MySQL Workbench para importar o script do banco de dados para o servidor MySQL. Ou utilize o utilitário `mysql` para importar o banco de dados. Por exemplo:

Workbench
<img src="readElements/ImportScript.png" alt="">

Terminal
   ```bash
   mysql -u seu_nome_de_usuario -p nome_do_banco_de_dados < script_banco_de_dados.sql
   ```
### 3. Como Executar

Execute a classe principal `HelloApplication.java` que contém o método `main` para iniciar o programa, localizada em:
```
TODOLIST/src/main/java/com/example/TODOTODOLIST/HelloApplication.java
```

## 🔍Como Usar a Interface Gráfica

A interface gráfica do aplicativo Todo List oferece uma maneira intuitiva de gerenciar suas tarefas. Aqui está um guia rápido sobre como usá-la:

1. **Página Login**: Esta é a primeira página a ser acessada. Aqui você pode navegar até a página de registro para criar um novo usuário ou acessar a sua página inicial de tarefas caso já tenha uma conta.

   ![Página de Login](readmeElements/loginPage.png)

2. **Página de Registro**: Nesta página você pode registrar um novo usuário.

   ![Página de Registro](readmeElements/imgRegister.png)
   Tenha atenção ao criar uma senha, sua senha deve seguir estas orientações:
   - Pelo menos 12 caracteres
   - Pelo menos um dígito
   - Pelo menos uma letra minúscula
   - Pelo menos uma letra maiúscula
   - Pelo menos um caractere especial
   
<hr>

3. **Homepage**: Ao realizar login, você terá acesso à homepage onde poderá visualizar, adicionar, editar e excluir tarefas. Além disso, é possível acessar a página de configuração do usuário ou retornar ao login.

   ![Página de Homepage](readmeElements/imgHomepage.png)

- **Adicionar Tarefa**:
  <br>
  ![Página de Adicionar Tarefa](readmeElements/imgAddTask.png)

  Ao clicar na opção "Add Task", uma pequena janela será exibida para que as informações da tarefa sejam incluídas. Você poderá inserir quantas tarefas quiser e, ao terminar a inserção, feche a janela para que a lista de tarefas seja atualizada com as novas tarefas.

- **Editar Tarefa**:
  <br>
  ![Menu de Contexto](readmeElements/imgMenuContent.png)
  ![Página de Edição de Tarefa](readmeElements/imgEditTask.png)

  Ao clicar com o botão direito do mouse em uma tarefa, você poderá visualizar opções para gerenciar suas tarefas. A primeira delas é a "Edit Task", que como o nome sugere, permite editar os valores da sua tarefa. Ao clicar nesta opção, uma pequena janela será exibida com as informações preenchidas da sua tarefa para edição.


- **Excluir Tarefa**: Como demonstrado anteriormente, a exclusão de uma tarefa pode ser feita através do botão direito do mouse. Ao excluir uma tarefa, a página será atualizada automaticamente.

- **Finalizar Tarefa**: Ao clicar na opção "Change Status", mostrada anteriormente, o status de sua tarefa poderá ser atualizado para "In progress" ou "Done".

<hr>

3. **Configurações**: Esta página destina-se a alterações na conta do usuário. Aqui, o usuário poderá editar todas as informações de sua conta. Tenha em mente que todos os campos devem ser preenchidos para que a modificação seja possível. Além disso, o usuário é capaz de deletar sua conta caso deseje, com isso sua conta e suas tarefas serão deletadas em efeito cascata.

   ![Página de Configurações](readmeElements/imgSettings.png)

<hr>
Com essas funcionalidades básicas, você poderá gerenciar suas tarefas de forma eficaz usando a interface gráfica do aplicativo Todo List.

## ⚙️ Testes
Os testes realziados estão presentes em TODOLIST/Testes/Testes.xlsx

## 👏 Agradecimentos

Agradeço à minha tutora, Tatiana Pereira Filgueiras, e ao meu professor, Wandré Nunes de Pinho Veloso, por sua orientação e suporte ao longo deste projeto. Especialmente, gostaria de agradecer à professora Tatiana
por indicar e demonstrar didaticamente como poderíamos utilizar ferramentas no ambiente Java para desenvolver da melhor forma possível este projeto.


## 🧰 Tecnologias Utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias:

<p><img src="readmeElements/imgJava.png" alt="" width="40"> Java 21</p>
<p><img src="readmeElements/imgJavaFX.png" alt="" width="40"> JavaFX 20</p>
<p><img src="readmeElements/imgMySQL.png" alt="" width="40"> MySQL</p> 

